# postfix relay with sasl authentication and postfwd rate limit rules

preparing before docker-compose up

## generate self-sign certificate for postfix smtpd tls

```
mkdir postfix/tls
cd postfix/tls
openssl req  -nodes -x509 -newkey rsa:2048 -keyout key.pem -out cert.pem -sha256 -days 3650
```

## create password database for dovecot that use for postfix sasl authentication
```
cd ../..
mkdir dovecot/passwd-file
cd dovecot/passwd-file
touch passwd
```

use utility for generate CRYPT password
passworddb for dovecot
```
docker run --rm -it dovecot/dovecot doveadm pw
```

edit **passwd** file for exmaple use password from utility as well
```
user1:$2y$05$Vl3ngLjbht66Py22.Fp1XOrdO3DwFkDa1wgbN3YOZuS/Mxo6ty2Pq
user2:$2y$05$IUtgcLefbwTQ/1b5/ROnouu.fZzNJ.O9WLhrUbTdrKcwYUwflPK/q
user3:$2y$05$nyjFZxtBNGe70rzWDRWqBOsocXgIJ3NA.7AwwklmoRpGsuQ/7gIby
user4:{PLAIN}hello
```
please look https://doc.dovecot.org/configuration_manual/authentication/password_databases_passdb/ for more information

after edit **passwd** file you have to restart dovecot container to reload database
```
docker restart dovecot
```

## edit postfwd rule 
you can edit file **postfwd/config/postfwd.cf** for postfwd rule as you wish. this project come with rate limit 120 rcpts per hour per sasl user or 3000 rcpts per day per sasl user.

for more information please look https://www.postfwd.org/

## environment variables
edit **.env** for docker-compose environment variables
```
cp .env.dist .env
```
please look **docker-compose.yml** for required environment variables.

## start stack
```
docker-compose up -d
```
have a nice day.

# example code for send email

### nodejs
```
const nodemailer = require('nodemailer');

const main = async () => {
    const transporter = nodemailer.createTransport({
        host: process.env.MTA_HOST,
        port: 587,
        secure: false,
        requireTLS: true,
        auth: {
            user: "username",
            pass: "******************"
        },        
        tls: {
            rejectUnauthorized: false
        }
    });

    const body = `This is the HTML message body <b>in bold!</b>`;

    const info = await transporter.sendMail({
        from: '"SUPAWIT WANNAPILA <no-reply-supawit-poc@cmu.ac.th"',
        replyTo: 'supawit.w@cmu.ac.th',
        to: process.env.RECIPIENT,
        subject: `${new Date()} -Here is the subject`,
        html: body
    });

    console.log("Message sent: %s",JSON.stringify(info));

}
main().catch(console.error);
```

### php
```
<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

$mail = new PHPMailer(true);

try {
    //Server settings
    $mail->isSMTP();
    $mail->Host         = getenv("MTA_HOST");
    $mail->SMTPAuth     = true;
    $mail->Username     = "username";
    $mail->Password     = "********";
    $mail->Port         = 587;
    $mail->SMTPSecure   = true;
    $mail->SMTPAutoTLS = true;
    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );

    //Recipients
    $mail->setFrom('no-reply-supawit-poc@cmu.ac.th');
    $mail->addReplyTo('supawit.w@cmu.ac.th');
    $mail->addAddress(getenv("RECIPIENT"));

    //Content
    $mail->isHTML(true);                                  
    $mail->Subject = time().'-Here is the subject';
    $mail->Body    = 'This is the HTML message body <b>in bold!</b>';

    $mail->send();
    echo 'Message has been sent';
} catch (\Throwable $th) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}
```
see also https://gitlab.com/supawit/mta-poc